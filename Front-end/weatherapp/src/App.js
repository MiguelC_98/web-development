import { useEffect, useRef } from "react";
import weatherUrlForCity from "./api";
import asyncUpdateAction from "./actions/update-action.js";
import { useDispatch, useSelector } from "react-redux";

function App() {
  // const fetchWeather = async () => {
  //   try {
  //   const weatherData = await axios.get(WEATHER_URL)
  //   console.log(weatherData)
  // } catch(error) {
  //   console.log(error)
  // }
  // fetchWeather()

  const cityInputRef = useRef(null);

  const weatherData = useSelector((state) => state);

  const dispatch = useDispatch();

  const fetchWeatherData = (dispatch) => {
    dispatch(asyncUpdateAction(weatherUrlForCity(cityInputRef.current.value)));
  };

  useEffect(() => {
    fetchWeatherData(dispatch);
  }, [dispatch]);

  const onSubmitHandler = (event) => {
    event.preventDefault();
    fetchWeatherData(dispatch);
  };

  return (
    <div className="App">
      <form>
        <input type="text" defaultValue="Lisbon" ref={cityInputRef} />
        <input
          type="submit"
          value="Update"
          style={{ marginLeft: "10pt" }}
          onClick={onSubmitHandler}
        />
      </form>
      <p>Code: {weatherData.code}</p>
      <img src={weatherData.icon} alt={weatherData.code} />
      <h3>{weatherData.text}</h3>
    </div>
  );
}

export default App;
