import './styles/App.scss'
import Player from './components/Player.js'
import Song from "./components/Song.js"
import Library from './components/Library.js'
import data from './data.js'
import Nav from './components/Nav.js'
import { useState } from 'react'


function App() {

  //Setting states -----------------------------------------------------------------------

  const [songs,setSongs] = useState(data());
  const[currentSong,setCurrentSong] = useState(songs[0]);
  const[isLibraryOpen,setisLibraryOpen] = useState(false);
  const[isSongActive,setisSongActive] = useState(false);  

  //Setting states ends here --------------------------------------------------------------
  //Handlers ------------------------------------------------------------------------------

  const openLibraryHandler = (event) => {
    setisLibraryOpen(!isLibraryOpen);
  }

  const notifyActiveLibraryHandler = (nextPrev) => {
    const newSongs = songs.map((song) => {
        if (song.id === nextPrev.id){
            return{
                ...song,
                active:true
            }
        } else {
            return{
                ...song,
                active:false
            }
        }
    })
    setSongs(newSongs);
}

  const songPick = (event) => {
    for (let i=0; i<songs.length;i++){
      if (songs[i].name===event.target.id) {
        setisSongActive(true);
        setCurrentSong(songs[i]);
        notifyActiveLibraryHandler(songs[i]);
        document.documentElement.style.setProperty('--song-color-one', String(currentSong.color[0]));
        document.documentElement.style.setProperty('--song-color-two', String(currentSong.color[1]));
        break
      } else {
        continue;
      }
    }
  }

  //Handlers end here ---------------------------------------------------------------------------
  //CSS variables -------------------------------------------------------------------------------
  
  document.documentElement.style.setProperty('--song-color-one', String(currentSong.color[0]));
  document.documentElement.style.setProperty('--song-color-two', String(currentSong.color[1]));

  //CSS variables end here-----------------------------------------------------------------------
  //HTML code------------------------------------------------------------------------------------

  return (
    <div className={`App ${isLibraryOpen ? 'library-active':''}`}  >
        <Nav openLibraryHandler={openLibraryHandler}/>
        <Song currentSong={currentSong} />
        <Player currentSong={currentSong} 
          setCurrentSong={setCurrentSong} 
          isSongActive={isSongActive} 
          setisSongActive={setisSongActive} 
          songs={songs} 
          notifyActiveLibraryHandler={notifyActiveLibraryHandler} />
        <Library allSongs={songs} 
          isLibraryOpen={isLibraryOpen} 
          songPick={songPick} 
          currentSong={currentSong} />
    </div>
  );
}

//HTML code ends here ---------------------------------------------------------------------------

export default App;
