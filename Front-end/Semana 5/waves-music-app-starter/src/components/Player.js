import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { faPlay, faPause,faAngleRight,faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { useRef , useState } from 'react'
import '../styles/Player.scss'



const Player = ({currentSong,
                isSongActive,
                setisSongActive,
                songs,
                setCurrentSong,
                notifyActiveLibraryHandler
            }) => {

    //States, Refs, Constants, etc. ---------------------------------------------------------------
    const animateTrackRef=useRef(null);
    const audioRef=useRef(null);
    const [songInfo,setsongInfo] = useState({
        current: 0,
        duration: 0,
    });
    const animationPercentage = Math.round((songInfo.current / songInfo.duration)*100)

    //States, Refs, Constants, etc. ends here ----------------------------------------------------
    //Function to make time pretty ---------------------------------------------------------------

    const getTime = (time) =>{
        return (
            Math.floor(time/60)+":"+("0"+Math.floor(time%60)).slice(-2)
        )
    }

    //Function to make time pretty ends here -----------------------------------------------------
    //Handlers -----------------------------------------------------------------------------------

    const onTimeUpdateHandler = (event) => {
        const current = event.target.currentTime;
        const duration = event.target.duration;
        setsongInfo({
            current:current,
            duration:duration,
        }) 
    }

    
    const onEndedHandler = async (event) => {
        const songsCount=songs.length;
        const currentSongIndex = songs.findIndex((song) => {
            return song.id === currentSong.id;
        });
        let nextIndex;
        if (currentSongIndex === (songsCount-1)) {
            nextIndex = 0;
        } else {
            nextIndex = currentSongIndex+1
        }
        await setCurrentSong(songs[nextIndex]);
        //console.clear();
        notifyActiveLibraryHandler(songs[nextIndex]);
        animateTrackRef.current.style={transform:`translateX:0%)`};
        setisSongActive(true);
    }
    
    const playPauseHandler = (event) => {
        let aud = audioRef.current;
        if (aud.paused){
            aud.play();
            setisSongActive(!isSongActive);
        } else {
            aud.pause();
            setisSongActive(!isSongActive);
        }
    }

    const audioTimeChangeHandler = (event) => {
        audioRef.current.currentTime=event.target.value;
        setsongInfo({...songInfo, current: event.target.value});
    }
    
    const skipTrackHandler = async (forwards) => {
        const songsLength = songs.length;
        const currentSongIndex = songs.findIndex((song) => {
            return song.id === currentSong.id;
        });
        let nextIndex;
        if (forwards){
            if (currentSongIndex === (songsLength-1)) {
                nextIndex = 0;
            } else {
                nextIndex = currentSongIndex+1
            }
        } else {
            if (currentSongIndex === 0) {
                nextIndex = (songsLength-1);
            } else {
                nextIndex = currentSongIndex-1;
            }
        }
        await setCurrentSong(songs[nextIndex]);
        notifyActiveLibraryHandler(songs[nextIndex]);
        //console.clear();
        animateTrackRef.current.style={transform:`translateX:0%)`};
        setisSongActive(true);
    }

    //Handlers end here ---------------------------------------------------------------------
    //CSS variables -------------------------------------------------------------------------

    document.documentElement.style.setProperty('--song-color-one', String(currentSong.color[0]));
    document.documentElement.style.setProperty('--song-color-two', String(currentSong.color[1]));

    //CSS variables end here ----------------------------------------------------------------
    //Making sure song starts ---------------------------------------------------------------
    
    if (isSongActive){
        audioRef.current.play()
    }
    
    //Making sure song starts ends here ------------------------------------------------------
    //HTML code ------------------------------------------------------------------------------

    return(
        <div className="player">
            <div className="time-control">
                <p>{getTime(songInfo.current)}</p>
                <div className="track" style = {{
                   backgroundImage:`linear-gradient(to right, ${currentSong.color[0]},${currentSong.color[1]})`,
                   backgroundColor:`#c7c4c4`
                }}>
                    <input type="range" max={songInfo.duration} min="0" value={songInfo.current} onChange={audioTimeChangeHandler}/>
                    <div className="animate-track" ref={animateTrackRef} style={{transform:`translateX(${(animationPercentage - (0.02999999999999999*animationPercentage))}%)`}}><div className="animate-track-thumb" ></div></div>
                </div>
                <p>{getTime(songInfo.duration)}</p>
            </div>
            <div className="play-control">
                <FontAwesomeIcon icon={faAngleLeft} size='2x' onClick={() => {skipTrackHandler(false)}} />
                <FontAwesomeIcon icon={isSongActive ? faPause:faPlay} size='2x' onClick={playPauseHandler} />
                <FontAwesomeIcon icon={faAngleRight} size='2x' onClick={() => {skipTrackHandler(true)}} />
                <audio src={currentSong.audio} ref={audioRef} onTimeUpdate={onTimeUpdateHandler} onLoadedMetadata={onTimeUpdateHandler} onEnded={onEndedHandler}/>
            </div>
        </div>
    );
}

//HTML code ends here-----------------------------------------------------------------------

export default Player;