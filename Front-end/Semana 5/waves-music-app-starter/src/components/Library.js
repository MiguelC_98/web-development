import LibrarySong from './LibrarySong.js'



const Library = ({allSongs , isLibraryOpen, songPick,currentSong}) => {

    //CSS variables ------------------------------------------------------------------------------

    document.documentElement.style.setProperty('--song-color-one', String(currentSong.color[0]));
    document.documentElement.style.setProperty('--song-color-two', String(currentSong.color[1]));

    //CSS variables ends here --------------------------------------------------------------------
    //HTML code ----------------------------------------------------------------------------------

    return(
        <div className={`library ${isLibraryOpen ? 'open':''}`}>
            <h2>Library</h2>
            <div className="library-songs">
                {
                    allSongs.map((song) => {
                        return <LibrarySong song={song} songPick={songPick} />;
                    })
                }
            </div>
        </div>
    );
}

//HTML code ends here ----------------------------------------------------------------------------

export default Library;