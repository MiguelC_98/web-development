
import '../styles/Song.scss'

const Song = ({currentSong}) => {

    //CSS code -------------------------------------------------------------------------------------

    document.documentElement.style.setProperty('--song-color-one', String(currentSong.color[0]));
    document.documentElement.style.setProperty('--song-color-two', String(currentSong.color[1]));

    //CSS code ends here ---------------------------------------------------------------------------
    //HTML code ------------------------------------------------------------------------------------

    return (
        <div className="song">
            <img src={currentSong.cover} alt={currentSong.name}  />
            <h2>{currentSong.name}</h2>
            <h3>{currentSong.artist}</h3>
        </div>
    );
}

//HTML code ends here -----------------------------------------------------------------------------

export default Song;