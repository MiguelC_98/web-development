


const LibrarySong = ({song,songPick}) => {
    return(
        <div className={`library-song ${song.active ? 'selected' : ''}`} onClick={songPick} key={song.id}>
            <img src={song.cover} id={song.name} alt={song.name} />
            <div className="song-description" id={song.name}>
                <h3 id={song.name}>{song.name}</h3>
                <h4 id={song.name}>{song.artist}</h4>
            </div>
        </div>
    );
}

export default LibrarySong;