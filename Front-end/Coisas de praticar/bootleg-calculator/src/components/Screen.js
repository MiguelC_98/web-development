import '../styles/screen.scss'


const Screen = ({screenValue}) => {
    return(
        <div className="screen">
            <h1>{screenValue}</h1>
        </div>
    );

}
export default Screen;