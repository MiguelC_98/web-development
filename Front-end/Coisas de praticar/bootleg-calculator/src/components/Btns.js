import "../styles/Btns.scss"
import React, { useState } from 'react'
import useKeyPress from 'react-use-keypress'


const Btns = ({screenValue, setScreenValue,holdOperation,setHoldOperation}) => {

    const [holdValue,setHoldValue] = useState("");
    const [operationDone,setOperationDone] = useState(false);
    const [severalOperations,setSeveralOperations] = useState(false);


    const addNum = (event) => {
    if (operationDone){
        if (severalOperations){
            setHoldValue(screenValue);
        } else {
        }
        setScreenValue(event.target.innerHTML);
        setOperationDone(false);
        
    } else {
        let value = screenValue + event.target.innerHTML;
        setScreenValue(String(value));

    }
    }


    const clearHandler = () => {
        setScreenValue("");
        setHoldOperation();
        setOperationDone(false);
    }

    const changeValue = () => {
        let val = Math.sign(screenValue);
        if (val>0){
            setScreenValue("-"+screenValue);
        } else {
            let newVal= screenValue.replace("-","")
            setScreenValue(newVal);
        }
    }

    const percentage = () => {
        setScreenValue((screenValue/100));
    }

    const equal = () => {
        let value;
        if (holdOperation === "+"){
            value=Number(holdValue)+Number(screenValue);
        } else if (holdOperation === "-"){
            value=Number(holdValue)-Number(screenValue);
        } else if (holdOperation === "*"){
            value=Number(holdValue)*Number(screenValue);
        } else if (holdOperation === "/"){
            value=Number(holdValue)/Number(screenValue);
        } else {
        }
        setScreenValue(String(value));
        setOperationDone(true);
        setHoldOperation();
        }

    const operationPlus = (event) => {
        if (typeof holdOperation === 'undefined'){
            setHoldOperation("+");
            setHoldValue(screenValue);
            setScreenValue("");
        } else {
            setSeveralOperations(true);
            equal();
            setHoldOperation("+");
            setHoldValue(screenValue);
        }
    }
    
    const operationMinus = () => {
        if (typeof holdOperation === 'undefined'){
            setHoldOperation("-");
            setHoldValue(screenValue);
            setScreenValue("");
        } else {
            setSeveralOperations(true);
            equal();
            setHoldOperation("-");
            setHoldValue(screenValue);
        }
    }

    const operationMultiply = () => {
        if (typeof holdOperation === 'undefined'){
            setHoldOperation("*");
            setHoldValue(screenValue);
            setScreenValue("");
        } else {
            setSeveralOperations(true);
            equal();
            setHoldOperation("*");
            setHoldValue(screenValue);
        }
    }

    const operationDivision = () => {
        if (typeof holdOperation === 'undefined'){
            setHoldOperation("/");
            setHoldValue(screenValue);
            setScreenValue("");
        } else {
            setSeveralOperations(true);
            equal();
            setHoldOperation("/");
            setHoldValue(screenValue);
        }
    }



    const addNumWKey = (event) => {
        if (operationDone){
            if (severalOperations) {
                setHoldValue(screenValue)
            } else {
            }
            setScreenValue(event.key);
            setOperationDone(false);
        } else {
            let value = screenValue + event.key;
            setScreenValue(String(value));
        }
    }


    //Adding number with a key press
    useKeyPress(['0','1','2','3','4','5','6','7','8','9','Decimal'],addNumWKey);
    useKeyPress("Backspace",() => {
        setScreenValue(screenValue.slice(0,(screenValue.length-1)))
    });

    useKeyPress("*",operationMultiply);
    useKeyPress("+",operationPlus);
    useKeyPress("-",operationMinus);
    useKeyPress("/",operationDivision);
    useKeyPress("%",percentage);
    useKeyPress("Enter",equal);
    useKeyPress(".",addNumWKey);

    return(
        <div className="btns">
            <div className="row">
                <div className="col-sm"><button className="clear" onClick={clearHandler}>C</button></div>
                <div className="col-sm"><button className="pos-neg" onClick={changeValue}><sup>&#x2b;</sup>/<sub>&#x2212;</sub></button></div>
                <div className="col-sm"><button className="percentage" onClick={percentage}>%</button></div>
                <div className="col-sm"><button className="operator" onClick={operationDivision}>&#xf7;</button></div>
            </div>
            <div className="row">
                <div className="col-sm"><button className="seven" onClick={addNum}>7</button></div>
                <div className="col-sm"><button className="eight" onClick={addNum}>8</button></div>
                <div className="col-sm"><button className="nine" onClick={addNum}>9</button></div>
                <div className="col-sm"><button className="operator" onClick={operationMultiply}>&#xd7;</button></div>
            </div>
            <div className="row">
                <div className="col-sm"><button className="four" onClick={addNum}>4</button></div>
                <div className="col-sm"><button className="five" onClick={addNum}>5</button></div>
                <div className="col-sm"><button className="six" onClick={addNum}>6</button></div>
                <div className="col-sm"><button className="operator" onClick={operationMinus}>&#x2212;</button></div>
            </div>
            <div className="row">
                <div className="col-sm"><button className="one" onClick={addNum}>1</button></div>
                <div className="col-sm"><button className="two" onClick={addNum}>2</button></div>
                <div className="col-sm"><button className="three" onClick={addNum}>3</button></div>
                <div className="col-sm"><button className="operator" onClick={operationPlus}>&#x2b;</button></div>
            </div>
            <div className="row">
                <div className="col-lg"><button className="zero" onClick={addNum}>0</button></div>
                <div className="col-sm"><button className="decimal" onClick={addNum}>.</button></div>
                <div className="col-sm"><button className="operator" onClick={equal}>&#x3d;</button></div>
            </div>
        </div>
    );

}
export default Btns;