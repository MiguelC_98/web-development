import React from 'react'
import { useState } from 'react'
import './styles/App.scss';
import Screen from './components/Screen.js'
import Btns from './components/Btns.js'
function App() {

  const [screenValue,setScreenValue] = useState("");
  const [holdOperation,setHoldOperation] = useState();

  return (
    <div className="App">
      <Screen screenValue={screenValue}/>
      <Btns screenValue={screenValue} 
      setScreenValue={setScreenValue}
      holdOperation={holdOperation}
      setHoldOperation={setHoldOperation}/>
    </div>
  );
}

export default App;
