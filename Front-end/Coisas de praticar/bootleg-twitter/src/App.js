import TweetList from './components/TweetList'
import { useState , useRef } from 'react' //importar o use estado
import { uuid } from 'uuidv4'
import './styles/App.scss'

function App() {

  const inputRef= useRef(null);

  const [tweets,setTweets] = useState([]);  //definir o estado, setTweets é a função que nos permite alterar o estado


  const submitClickHandler = (event) => {
    event.preventDefault();
    const TweetText = {id:uuid(),"like":0,"text":inputRef.current.value};
    let newTweets = [...tweets,TweetText];
    setTweets(newTweets);
    inputRef.current.value="";

  }
  const appStyles = {
    color:"coral"
  }

  return (
    <div className="App" style={appStyles}>
      <form>
        <label>Start tweeting!</label><br></br>
        <input type="text" placeholder="Write your tweet here" ref={inputRef} />
        <button type="submit" id="button" onClick={submitClickHandler}>Tweet</button>
      </form>
      <TweetList tweets={tweets} setTweets={setTweets} />
    </div>
  );
}

export default App;
