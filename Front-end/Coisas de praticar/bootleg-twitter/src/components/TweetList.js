import Tweet from './Tweet'

const TweetList = ({tweets,setTweets}) => {
    
    return (
        <div className="tweet-list">
            {
                tweets.map((tweet) =>{
                    return <Tweet key={tweet.id} tweet={tweet} tweets={tweets} setTweets={setTweets}/>
                })
            }
        </div>
    )
}

export default TweetList;