import { useState } from 'react'
import s from '../styles/Tweet.module.scss'

const Tweet = ({tweet,tweets,setTweets}) => {

    const [tweetLikes,setTweetLikes] = useState(tweet.like);

    const removeTweetHandler = (event) => {
        const newTweets = tweets.filter((TweetItem) => {
            return TweetItem.id !== tweet.id;
        });
        setTweets(newTweets);
    }
    const likeTweetHandler = (event) => {
        tweet.like++;
        setTweetLikes(tweet.like);

    }
    const dislikeTweetHandler = (event) => {
        if (tweet.like === 0){
            //pass
        } else {
            tweet.like--;
        }

        setTweetLikes(tweet.like);
    }
    return(
        <div className={s.tweet}>
            <h3>{tweet.text}</h3>
            <button onClick={removeTweetHandler}>Remove</button>
            <button onClick={likeTweetHandler}><i className="far fa-heart"></i> </button>
            <h4 className={`${tweet.like>0 ? "":"hidden"}`}>{tweetLikes}</h4>
            <button onClick={dislikeTweetHandler}><i className="fas fa-heart-broken"></i></button>
        </div>
    );
}

export default Tweet;