import data from './data.js'
import MarketItems from './components/MarketItems.js'
import Nav from './components/Nav.js'
import ListBuy from './components/ListBuy.js'
import './styles/App.scss'
import { useState } from 'react'

function App() {

  const [allItems,setAllItems] = useState(data());
  const [cartItems,setCartItems] = useState([]);
  const [isCartOpen,setIsCartOpen] = useState(false);

  return (
    <div className="App">
      <Nav isCartOpen={isCartOpen} setIsCartOpen={setIsCartOpen}/>
      <ListBuy isCartOpen={isCartOpen} cartItems={cartItems} setCartItems={setCartItems} />
      <MarketItems allItems={allItems} cartItems={cartItems} setCartItems={setCartItems}/>
    </div>
  );
}

export default App;
