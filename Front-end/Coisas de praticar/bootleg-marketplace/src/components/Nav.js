import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { useState } from 'react'


const Nav = ({isCartOpen,setIsCartOpen}) => {

    const openCart = () =>{
        setIsCartOpen(!isCartOpen);
      }

    return(
        <div className="nav">
            <button onClick={openCart} className="shopBtn">Shopping Cart <FontAwesomeIcon icon={faShoppingCart} /></button>
        </div>
    )
}
export default Nav