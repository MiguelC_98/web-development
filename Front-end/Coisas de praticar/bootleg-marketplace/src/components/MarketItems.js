import MarketItem from './MarketItem'

const MarketItems = ({allItems,cartItems,setCartItems}) => {


    return(
        <div className="market-items">
            {allItems.map((item) => {
                return(<MarketItem item={item} cartItems={cartItems} setCartItems={setCartItems} />);
            })}
        </div>
    )
}

export default MarketItems;