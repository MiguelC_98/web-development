

const ListItem = ({cartItem,setCartItems,cartItems}) => {

    const removeItem = () => {
        let newItems=[]
        cartItems.map((item) => {
            if (item.id===cartItem.id) {
            } else {
                newItems.push(item);
            }
            return newItems;
        })
        setCartItems(newItems);
    }

    return(
        <div className="list-item" id={cartItem.id}>
            <img src={cartItem.photo} alt={cartItem.name} />
            <h2>{cartItem.name}</h2>
            <h4>{cartItem.price}€</h4>
            <button onClick={removeItem} >Remove</button>
            <p>{cartItem.quantity}</p>
    </div>
    );
}

export default ListItem;