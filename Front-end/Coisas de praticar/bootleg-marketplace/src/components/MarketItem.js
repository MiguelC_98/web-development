import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'


const MarketItem = ({item,cartItems,setCartItems}) => {


    const addItem= () =>{
        let cartList=[]
        cartItems.map((cartItem) => {
            return(cartList.push(cartItem));
        })
        let newCartItem={}
        if ((typeof item.quantity) === 'undefined') {
            newCartItem={...item,'quantity':1}
            item.quantity=1
        } else {
            newCartItem={...item,'quantity':item.quantity+1}
            item.quantity=item.quantity+1
        }
        cartList.push(newCartItem);
        setCartItems(cartList);
        console.log(newCartItem);
    };

    return(
    <div className="market-item" id={item.id}>
        <img src={item.photo} alt={item.name} />
        <div className="item-description">
            <h2>{item.name}</h2>
            <h4>{item.price}€</h4>
            <button onClick={addItem}><FontAwesomeIcon icon={faShoppingCart} /></button>
            <p>{item.description}</p>
        </div>
    </div>
    );
}

export default MarketItem;