import ListItem from './ListItem'

const ListBuy = ({isCartOpen,cartItems,setCartItems}) => {
    return(
        <div className={`cart ${isCartOpen ? 'open':''}`}>
            {cartItems.map((cartItem) => {
                return(<ListItem cartItem={cartItem} setCartItems={setCartItems} cartItems={cartItems} />);
            })}
        </div>
    )
}
export default ListBuy;