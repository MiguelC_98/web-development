import { uuid } from 'uuidv4'

function data() {
    return[
        {name:"Washing Machine",
        price:"499.99",
        photo:"https://www.lg.com/in/images/washing-machines/md07515048/gallery/FHM1207ZDL-Washing-Machines-Front-View-D-01.jpg",
        description:"Washes items, very washy washy.",
        id:uuid()},
        {name:"Knife",
        price:"4.99",
        photo:"https://www.ikea.com/pt/en/images/products/ikea-365-cooks-knife-stainless-steel__0710374_PE727502_S5.JPG?f=s",
        description:"It cuts things, like vegetables... But only... Physical things not things like... Dreams.",
        id:uuid()},
        {name:"Book",
        price:"9.99",
        photo:"https://dictionary.cambridge.org/pt/images/full/book_noun_001_01679.jpg?version=5.0.141",
        description:"It's not a very good book... Doesn't even have an author... Or a name...",
        id:uuid()},
        {name:"Lamp",
        price:"19.99",
        photo:"https://www.ikea.com/us/en/images/products/milleryr-table-lamp-with-led-bulb-white-nickel-plated__0744750_PE743379_S5.JPG?f=s",
        description:"Lightbulb not included.",
        id:uuid()},
        {name:"Lighter",
        price:"0.99",
        photo:"https://images-na.ssl-images-amazon.com/images/I/61K0ioY6NqL._SL1200_.jpg",
        description:"Very good for lighting something... But not to create light.",
        id:uuid()}
    ];
}

export default data;