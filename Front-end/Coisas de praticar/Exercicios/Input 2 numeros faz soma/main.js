document.getElementById("add").addEventListener("click", function () {
  var num1 = Number(document.getElementById("num1").value);
  var num2 = Number(document.getElementById("num2").value);
  var total;
  total = num1 + num2;
  var sentence = document.createElement("p");
  var history = String(num1) + " + " + String(num2) + " = " + String(total);
  var things = document.createTextNode(history);
  sentence.appendChild(things);
  document.getElementById("history").prepend(sentence);
  document.getElementById("num1").value = String("");
  document.getElementById("num2").value = String("");
});
