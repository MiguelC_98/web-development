function domath(operator) {
  var num1 = Number(document.getElementById("num1").value);
  var num2 = Number(document.getElementById("num2").value);
  var operators = {
    "+": function (num1, num2) {
      return num1 + num2;
    },
    "-": function (num1, num2) {
      return num1 - num2;
    },
    "*": function (num1, num2) {
      return num1 * num2;
    },
    "/": function (num1, num2) {
      return num1 / num2;
    },
  };
  var operation = operators[operator];
  total = operation(num1, num2);
  var sentence = document.createElement("p");
  var history =
    String(num1) +
    " " +
    String(operator) +
    " " +
    String(num2) +
    " = " +
    String(total);
  var things = document.createTextNode(history);
  sentence.appendChild(things);
  document.getElementById("history").prepend(sentence);
  document.getElementById("total").innerHTML = String(total);
  document.getElementById("num1").value = String("");
  document.getElementById("num2").value = String("");
}
document.getElementById("add").addEventListener("click", function () {
  domath("+");
});
document.getElementById("sub").addEventListener("click", function () {
  domath("-");
});
document.getElementById("mul").addEventListener("click", function () {
  domath("*");
});
document.getElementById("div").addEventListener("click", function () {
  domath("/");
});
