import home1 from "../images/home1.png"
import {StyledSection,StyledDescription,StyledHide,StyledImage} from "../Styles"

const AboutSection = () => {
    return(
        <StyledSection>
            <StyledDescription>
                <div className="title">
                    <StyledHide>
                        <h2>We work to make</h2>
                    </StyledHide>
                    <StyledHide>
                        <h2>your <span>dreams</span></h2>
                    </StyledHide>
                    <StyledHide>
                        <h2>come true.</h2>
                    </StyledHide>
                </div>
                <p>Contact us for any photography or videography ideas that you have. We have professionals with amazing skills.</p>
                <button>
                    Contact us
                </button>
            </StyledDescription>
            <StyledImage>
                <img src={home1} alt="guy with a camera" />
            </StyledImage>
        </StyledSection>
    );
}


export default AboutSection;