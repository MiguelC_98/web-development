import {StyledSection} from "../Styles"
import styled from "styled-components"


const FaqSection = () => {
    return(
        <StyledFaqSection>
            <h2>Any questions? <span>FAQ</span></h2>
            <div className="question">
                <h4>How do i start?</h4>
                <div className="answer">
                    <p>Lorem ipsum dolor, sit amet.</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil tempore, totam consequatur, architecto odit quasi sunt porro quae hic deserunt quo, itaque fugiat cupiditate. Repellendus provident neque consequuntur laudantium sunt!</p>
                </div>
                <div className="faq-line">
                    
                </div>
            </div>
            <div className="question">
                <h4>Daily Schedule</h4>
                <div className="answer">
                    <p>Lorem ipsum dolor, sit amet.</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil tempore, totam consequatur, architecto odit quasi sunt porro quae hic deserunt quo, itaque fugiat cupiditate. Repellendus provident neque consequuntur laudantium sunt!</p>
                </div>
                <div className="faq-line">
                    
                </div>
            </div>
            <div className="question">
                <h4>Different Payment Methods</h4>
                <div className="answer">
                    <p>Lorem ipsum dolor, sit amet.</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil tempore, totam consequatur, architecto odit quasi sunt porro quae hic deserunt quo, itaque fugiat cupiditate. Repellendus provident neque consequuntur laudantium sunt!</p>
                </div>
                <div className="faq-line">
                    
                </div>
            </div>
            <div className="question">
                <h4>What products do you offer?</h4>
                <div className="answer">
                    <p>Lorem ipsum dolor, sit amet.</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil tempore, totam consequatur, architecto odit quasi sunt porro quae hic deserunt quo, itaque fugiat cupiditate. Repellendus provident neque consequuntur laudantium sunt!</p>
                </div>
                <div className="faq-line">

                </div>
            </div>
        </StyledFaqSection>
    );
}

const StyledFaqSection = styled(StyledSection)`
    display: block;
    span {
        display: block;
    }
    h2 {
        padding-bottom: 2rem;
        font-weight: lighter;
    }
    .faq-line {
        background-color: #cccccc;
        height: 0.2rem;
        margin: 2rem 0rem;
        width: 100%;
    }

    .question {
        padding: 3rem 0rem;
        cursor: pointer;
    }

    .answer{
        padding: 2rem 0rem;

        p{
            padding: 1rem 0rem;
        }
    }
`

export default FaqSection;