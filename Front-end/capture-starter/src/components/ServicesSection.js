import home2 from "../images/home2.png";
import clock from "../images/clock.svg";
import teamwork from "../images/teamwork.svg";
import diaphragm from "../images/diaphragm.svg";
import money from "../images/money.svg";
import {StyledSection,StyledDescription,StyledImage} from "../Styles";
import styled from 'styled-components';


const ServicesSection = () => {
    return(
        <StyledServicesSection>
            <StyledDescription>
                <h2>High <span>quality</span> services</h2>
                <StyledCards>
                    <StyledCard>
                        <div className="icon">
                            <img src={clock} alt="clock"/>
                            <h3>Efficient</h3>
                        </div>
                        <p>Lorem ipsum dolor.</p>
                    </StyledCard>
                    <StyledCard>
                        <div className="icon">
                            <img src={teamwork} alt="Teamwork"/>
                            <h3>Teamwork</h3>
                        </div>
                        <p>Lorem ipsum dolor.</p>
                    </StyledCard>
                    <StyledCard>
                        <div className="icon">
                            <img src={diaphragm} alt="diaphragm"/>
                            <h3>Diaphragm</h3>
                        </div>
                        <p>Lorem ipsum dolor.</p>
                    </StyledCard>
                    <StyledCard>
                        <div className="icon">
                            <img src={money} alt="Money Honey"/>
                            <h3>Affordable</h3>
                        </div>
                        <p>Lorem ipsum dolor.</p>
                    </StyledCard>
                </StyledCards>
            </StyledDescription>
            <StyledImage>
                <img src={home2} alt="camera" />
            </StyledImage>
        </StyledServicesSection>
    );
}

const StyledServicesSection = styled(StyledSection)`
    h2 {
        padding-bottom: 5rem;
    }

    p {
        width: 70%;
        padding: 2rem 0rem 4rem 0rem;
        
    }
`

const StyledCards = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 0;

`

const StyledCard = styled.div`
    flex-basis: 20rem;

    .icon {
        display: flex;
        align-items: center;
        
        h3 {
            margin-left: 1rem;
            background: white;
            color: black;
            padding: 1rem;
        }
    }
    @media screen and (max-width: 1600px) {
        flex-basis:15rem !important;
    }
` 



export default ServicesSection;