import { createGlobalStyle } from 'styled-components'


const GlobalStyles = createGlobalStyle`
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans&display=swap');
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Open Sans', sans-serif;

    }

    body {
        background-color: #1b1b1b;
    }

    button {
        font-weight: bold;
        font-size: 1.1rem;
        cursor: pointer;
        padding: 1rem 2rem;
        border: 3px solid #23d997;
        background-color: transparent;
        color:white;
        transition: all 0.5s ease;
        &:hover {
            background-color: #23d997;
        }

    }

    h2 {
        font-weight: lighter;
        font-size: 3.5rem;
    }

    h3 {
        color:white;
    }
    
    h4 {
        font-weight: bold;
        font-size:1.9rem;
    }

    span {
        font-weight: bold;
        color: #23d997;
    }

    p {
        padding: 3rem 0rem;
        color: #ccc;
        font-size: 1.2rem;
        letter-spacing: 1.5px;
        line-height: 150%;
    }
`

export default GlobalStyles;