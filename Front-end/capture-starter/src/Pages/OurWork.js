import athlete from "../images/athlete-small.png"
import theracer from "../images/theracer-small.png"
import goodtimes from "../images/goodtimes-small.png";
import styled from 'styled-components'
import { Link } from "react-router-dom";


const OurWork = () => {
    return(
        <StyledWork>
            <StyledMovie>
                <h2>The Athlete</h2>
                <div className="line"></div>
                <Link to="/the-athlete">
                    <img src={athlete} alt="athlete" />
                </Link>
            </StyledMovie>
            <StyledMovie>
                <h2>The Racer</h2>
                <div className="line"></div>
                <Link to="/the-racer">
                    <img src={theracer} alt="the-racer" />
                </Link>
            </StyledMovie>
            <StyledMovie>
                <h2>Good times</h2>
                <div className="line"></div>
                <Link to="/good-times">
                    <img src={goodtimes} alt="goodtimes" />
                </Link>
            </StyledMovie>
        </StyledWork>
    );
}

const StyledWork = styled.div`
    min-height:100vh;
    overflow: hidden;
    padding: 5rem 10rem;

    h2 {
        padding: 1rem 0rem;
        color: white;
    }
`

const StyledMovie = styled.div`
    padding-bottom: 10rem;

    .line {
        height: 0.5rem;
        background-color:#cccccc;
        margin-bottom: 3rem;
    }

    img {
        width:100%;
        height: 80vh;
        object-fit:cover;
    }

`

export default OurWork;