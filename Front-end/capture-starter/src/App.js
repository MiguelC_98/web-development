import AboutUs from "./Pages/AboutUs"
import OurWork from "./Pages/OurWork"
import ContactUs from "./Pages/ContactUs"
import MovieDetail from "./Pages/MovieDetail"
import GlobalStyles from "./components/GlobalStyles"
import { Switch, Route } from 'react-router-dom'
import Nav from "./components/Nav"

function App() {
  return (
    <div className="App">
      <GlobalStyles />
      <Nav />
      <Switch>
        <Route path="/" exact>
          <AboutUs />
        </Route>
        <Route path="/work" exact>
          <OurWork />
        </Route>
        <Route path="/work/:id">
          <MovieDetail />
        </Route>
        <Route path="/contact" exact>
          <ContactUs />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
