
const initialState = {
    loggedIn: false
}

export default function usersReducer(state=initialState, action) {
    switch (action.type) {
        case "LOGIN":
            return {...state, loggedIn: true}
        default:
            return {...state}
    }
}