import { combineReducers } from "redux";
import countReducer from "./count-reducer";
import usersReducer from "./users-reducer";

const rootReducers = combineReducers({
    count: countReducer,
    users: usersReducer
});

export default rootReducers;