import { useDispatch, useSelector } from "react-redux";
import addAction from "./actions/add-action";
import logIn from "./actions/login-action";
import subAction from "./actions/subtract-action";
import Message from "./components/message";
import Multiplier from "./components/multiplier";

function App() {
  const dispatch = useDispatch();

  const count = useSelector(state => state.count);
  const {loggedIn} = useSelector(state => state.users);
  console.log(loggedIn);
  return (
    <div className="App">
      <h2>Stefan is {loggedIn ? "" : "not " }logged in</h2>
      <h1>Count: {count}</h1>
      <button type="button" onClick={() => dispatch(addAction())}>+</button>
      <button type="button" onClick={() => dispatch(subAction())} style={{marginLeft: "20pt"}}>-</button>
      <Multiplier />
      <Message />
      <button type="button" onClick={() => dispatch(logIn())}>Log In</button>
    </div>
  );
}

export default App;
