import { useSelector } from "react-redux";

export default function Message() {

    const count = useSelector(state => state.count);

    return (
        <div className="multipler">
            <p>I know how to count until {count}</p>
        </div>
    )

}