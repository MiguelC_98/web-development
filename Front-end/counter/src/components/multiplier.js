import { useRef } from "react";
import { useDispatch } from "react-redux"
import multiplyAction from "../actions/multiply-action";

export default function Multiplier() {
    const dispatch = useDispatch();

    // let input = null;

    // useEffect(() => {
    //     input = document.querySelector("input");
    // }, []);

    let inputRef = useRef(null);
    
    const onClickHandler = () => {
        const value = Number(inputRef.current.value);
        dispatch(multiplyAction(value));
    }

    return (
        <div className="multiplier" style={{marginTop: "20pt"}}>
            <button type="button" onClick={onClickHandler}>*</button>
            <input type="number" name="multiplier" style={{marginLeft: "20pt"}} ref={inputRef} defaultValue="0" />
        </div>
    )
} 