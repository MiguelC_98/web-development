import styled from "styled-components";

export default function Nav() {
    return (
        <StyledNav>
            <div className="nav-content">
                <h1 id="poke">Pokédex</h1>
                <div className="trapezium-nav">
                    <h1 id="filter">By Number</h1>
                </div>
            </div>
        </StyledNav>
    );
}

const StyledNav = styled.nav`
    position: absolute;
    width:100%;
    top: 5vh;
    height:10vh;
    background-color:rgba(0,0,0,0.125);
    z-index:5;

    div.nav-content {
        position: relative;
        width:100vw;
        height:100%;

        h1#poke{
            position:absolute;
            font-size:3rem;
            margin-top:2vh;
            margin-left:1.5vw;
            font-weight:lighter;
        }

        div.trapezium-nav {
            position: absolute;
            right:0;
            top:0;
            width:43.75vw;
            height:100%;
            background-color: #333333;
            clip-path: polygon(5.6% 0%, 100% 0, 100% 100%, 0% 100%);

            h1#filter{
                text-align:center;
                font-size: 2.5rem;
                font-weight:lighter;
                margin-top:2.5vh;
                color: white;
            }
        }
    }
`