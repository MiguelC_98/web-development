import styled from "styled-components"
import InfiniteScroll from 'react-infinite-scroller'
import { useDispatch, useSelector } from 'react-redux'
import loadActionAsync from "../actions/load-action";
import Pokemon from "./pokemon";
import { useState } from "react";


export default function PokemonData() {

    const pokemonData = useSelector(state => state.load.pokemon);

    const dispatch = useDispatch();

    const [hasMore, setHasMore] = useState(true);

    const fetchMorePokemon = () => {
        if (pokemonData.length >= 898) {
            setHasMore(false);
            console.log("done")
        } else {
            dispatch(loadActionAsync(pokemonData.length));
            console.log("doing")
        }
    }


    return(
        <StyledPokemonData className="pokemon-data">
            <InfiniteScroll 
            pageStart={0}
            loadMore={fetchMorePokemon}
            hasMore={hasMore}
            loader={<div key={0} className="loader"><h1>Loading...</h1></div>}
            useWindow={false}
            initialLoad={true}>
                {
                    pokemonData ? pokemonData.map(pokemon => <Pokemon pokemon={pokemon} />):<p></p>
                }
            </InfiniteScroll>
        </StyledPokemonData>
    )
}

const StyledPokemonData = styled.div`
    margin-left: 200pt;
    overflow-y:scroll;
    height:82vh;
`