import styled from "styled-components";
import { imageUrlForPokemonWithId } from "../api";

export default function Pokemon ({pokemon}) {
    const urlParts = pokemon.url.split('/')
    const pokemonId = urlParts[urlParts.length-2]
    const pokemonIdFormatted = `00${pokemonId}`.slice(-3);

    const onClickListener = (event) => {
        event.currentTarget.classList.toggle('selected')
    }

    return(
        <StyledPokemon className={pokemonId == 1 ?"selected":""} onClick={onClickListener}>
            <img src={imageUrlForPokemonWithId(pokemonId)} alt={pokemon.name} />
            <div className="name-number-container">
                <h1>{`No. ${pokemonIdFormatted}`}</h1><h2>{pokemon.name}</h2>
            </div>
        </StyledPokemon>
    );
}

const StyledPokemon = styled.div`
    
    margin-top:2rem;
    font-size:1.5rem;
    border-radius: 30pt;
    margin-left:1.5rem;
    padding: 0 1.5rem;
    position:relative;
    
    
    &.selected {
        background-color:#F0501F;
        box-shadow: 0 5pt 20pt rgba(0,0,0,0.3);
        h1, h2{   
            color:white;
        }
    }

    img{
        position:absolute;
    }

    div.name-number-container{
        display:flex;
        h1,h2{
            flex:1;
        }
    }
`