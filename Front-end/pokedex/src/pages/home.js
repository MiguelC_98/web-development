import styled from "styled-components";
import Nav from "../components/nav";
import PokemonData from "../components/pokemon-data";
import PokemonImage from "../components/pokemon-image";

export default function Home() {
  return (
  <StyledHome className="home">
    <Nav />
    <div className="shadow-container">
      <div className="trapezium">
      </div>
    </div>
    <div className="trapezium-2">
    </div>
    <div className="content">
      <PokemonImage />
      <PokemonData />
    </div>

  </StyledHome>
  );
}

const StyledHome = styled.main`
  min-height: 100vh;
  min-width: 100%;
  background: rgb(255, 220, 248);
  background: radial-gradient(
    circle at 0% 60%,
    rgba(255, 220, 248, 1) 0%,
    rgba(255, 255, 255, 1) 35%,
    rgba(225, 243, 243, 1) 70%
  );
  div.shadow-container {
    /* div.trapezium{
      position: absolute;
      top: 0;
      right: 0;
      height: 0;
      width: 65vw;
      height:0;
      border-bottom: 100vh solid #F3523B;
      border-left: 25vw solid transparent;
      z-index:0;

      &::before {
        content: '';
        position: absolute;
        display: blocK;
        height: 100vh;
        width: 35vw;
        right: 17.473vw;
        transform: skewX(153.65deg);
        box-shadow: -0.1px 0 100pt #949494;
      }
    } */
    filter: drop-shadow(-10pt 0 50pt rgba(0,0,0,0.4));
    div.trapezium {
      position: absolute;
      top: 0;
      right: 0;
      height: 100vh;
      width: 65vw;
      clip-path:  polygon(38.48% 0, 100% 0, 100% 100%, 0% 100%);
      background-color: #F3523B;
      // box-shadow: -10px 5px 10px #000000; 
  }
  }

  div.trapezium-2{
    position:absolute;
    top:0;
    right:0;
    height:100vh;
    width:50vw;
    clip-path: polygon(47% 0, 100% 0, 100% 100%, 0% 100%);
    background-color:#FA7247;
    z-index:0;
  }

  /* div.trapezium {
    position: absolute;
    top: 0;
    right: 0;
    height: 100vh;
    width: 65vw;
    clip-path:  polygon(40% 0, 100% 0, 100% 100%, 0% 100%);
    background-color: #F3523B;
    box-shadow: -10px 5px 10px #000000;
  } */

  div.content{
    height: 100vh;
    width: 100%;
    display: flex;
    z-index:2;

    &>div {
      margin-top:18vh;
      flex: 1;
      z-index:3;
    }
  }

`;
