import GlobalStyles from "./components/global-styles";
import { Route, Switch } from "react-router-dom";
import Home from "./pages/home";

function App() {
  return (
    <div className="App">
      <GlobalStyles />
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
