$(document).ready(function () {
  var squareIn = true;
  $("#switch").click(function () {
    if (squareIn) {
      $("#square").fadeTo(2000, 0.0);
    } else {
      $("#square").fadeTo(2000, 1.0);
    }
    squareIn = !squareIn;
  });
});
