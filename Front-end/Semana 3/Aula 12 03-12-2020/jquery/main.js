//vars
var button = $("#light");
var title = $("header").children().first();
var bulb = $("#light-bulb");
var color = $("#color-picker").val();
var text = $("p");
var body = $("body:not(#light-bulb)");

//on document load
$(document).ready(function () {
  title.css("color", "white");
  $("body").css("background-color", "black");
  $("#light-bulb").css("color", "black");
  var button = $("#light");
  button.click(Light);
});

//functions
function Light() {
  button.toggleClass("clicked");
  if (button.hasClass("clicked")) {
    body.css("background-color", color);
    bulb.css("color", color);
    button.text("Turn off the light");
    if (color == "red" || color == "blue") {
      text.css("color", "white");
      title.css("color", "white");
      bulb.css("-webkit-text-stroke", "2px white");
    } else {
      title.css("color", "black");
      bulb.css("-webkit-text-stroke", "2px black");
    }
  } else {
    button.text("Turn on the light");
    body.css("background-color", "black");
    title.css("color", "white");
    bulb.css("color", "black");
    text.css("color", "black");
    bulb.css("-webkit-text-stroke", "2px black");
  }
}
