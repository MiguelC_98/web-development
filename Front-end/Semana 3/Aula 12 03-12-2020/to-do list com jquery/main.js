var idCount;
var todoInput;
var todoList;
var todoButton;
var todoInput;

//Elements
$(document).ready(function () {
  todoInput = $(".todo-input:first");
  todoButton = $(".todo-insert:first");
  todoList = $(".todo-list:first");
  filterDropdown = $(".filter-todo");
  todoButton.click(addTodo);
  todoList.click(deleteCheck);
  filterDropdown.change(filterTodo);
});

//Functions
function addTodo(event) {
  event.preventDefault();

  let todoValue = todoInput.val();
  let todoCompleteValue = false;
  idCount++;
  let todoJson = {
    value: todoValue,
    complete: todoCompleteValue,
    id: `todo-${idCount}`,
  };
  buildItemWithViewHolder(todoJson);
  save(todoJson);
  todoInput.value = "";
}

function deleteCheck(event) {
  // obter o botao da lista que foi pressionado
  var pressedBtn = event.target;

  if (pressedBtn.classList[0] == "trash-button") {
    const parent = pressedBtn.parentElement;
    parent.classList.add("fall");
    parent.addEventListener("transitionend", function () {
      parent.remove();
      removeTodoWith(parent.id);
    });
  }

  if (pressedBtn.classList[0] == "check-button") {
    const parent = pressedBtn.parentElement;
    parent.classList.toggle("complete");
    updateTodoWith(parent.id);
  }
}

function filterTodo(event) {
  const todos = todoList.querySelectorAll(".todo");
  todos.forEach(function (todo) {
    switch (event.target.value) {
      case "all":
        todo.style.display = "flex";
        break;
      case "completed":
        if (todo.classList.contains("complete")) {
          todo.style.display = "flex";
        } else {
          todo.style.display = "none";
        }
        break;
      case "uncompleted":
        if (!todo.classList.contains("complete")) {
          todo.style.display = "flex";
        } else {
          todo.style.display = "none";
        }
        break;
    }
  });
}

function removeTodoWith(id) {
  let todoItems = JSON.parse(localStorage.getItem("todo"));
  todoItems = todoItems.filter(function (todoItem) {
    return todoItem.id != id;
  });
  localStorage.setItem("todo", JSON.stringify(todoItems));
}

function updateTodoWith(id) {
  //1. obter o elemento do local storage com o id
  let todoItems = JSON.parse(localStorage.getItem("todo"));
  const item = todoItems.filter(function (todoItem) {
    return todoItem.id == id;
  })[0];
  //2. alterar o valor do elemento encontrado
  item.complete = !item.complete;
  //3. guardar alteraçoes
  localStorage.setItem("todo", JSON.stringify(todoItems));
}

function save(todo) {
  let todoItems;
  if (localStorage.getItem("todo") == null) {
    todoItems = [];
  } else {
    todoItems = JSON.parse(localStorage.getItem("todo"));
  }
  // adicionar elementos ao fim da lista
  todoItems.push(todo);
  localStorage.setItem("todo", JSON.stringify(todoItems));
}

function loadItems() {
  if (localStorage.getItem("todo") != null) {
    let todoItems = JSON.parse(localStorage.getItem("todo"));
    todoItems.map(function (todoItem) {
      buildItemWithViewHolder(todoItem);
    });
  }
}

function buildItemWithViewHolder(viewHolder) {
  //criar o div
  const div = document.createElement("div");
  div.id = viewHolder.id;
  if (viewHolder.complete) div.classList.add("complete");
  /*var class_todo = document.createAttribute("class");
    divClass.value = "todo";                Outra forma de adicionar uma class 
    div.setAttribute(divClass);*/
  div.classList.add("todo");
  // criar o elemento da lista <li>
  const todoItem = document.createElement("li");
  todoItem.classList.add("todo-item");
  todoItem.innerHTML = viewHolder.value;
  div.append(todoItem);
  // Criar o botao de checkmark
  const checkBtn = document.createElement("button");
  checkBtn.classList.add("check-button");
  checkBtn.innerHTML = '<i class="far fa-check-square"></i>';
  div.append(checkBtn);
  // Criar o butao trash
  const trashBtn = document.createElement("button");
  trashBtn.classList.add("trash-button");
  trashBtn.innerHTML = '<i class="fas fa-trash-alt"></i>';
  div.append(trashBtn);
  //adicionar o div ao ficheiro
  todoList.append(div);
}

window.onpageshow = function () {
  if (localStorage.getItem("idCount") === null) {
    idCount = 0;
  } else {
    idCount = JSON.parse(localStorage.getItem("idCount"));
  }
  loadItems();
};

window.onpagehide = function () {
  localStorage.setItem("idCount", JSON.stringify(idCount));
};
