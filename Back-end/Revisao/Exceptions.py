def exception_check(jogada):
    if len(jogada) > 2:
        raise Exception("Jogada inválida: Foram inseridos mais do que dois valores")
    elif len(jogada) < 2:
        raise Exception("Jogada inválida: Foram inseridos menos do que dois valores")
    elif jogada[0].isnumeric() is False or jogada[1].isnumeric() is False:
        raise Exception("Jogada inválida: Não foram inseridos números inteiros")
    elif int(jogada[0]) > 3 or int(jogada[1]) > 3:
        raise Exception("Jogada inválida: Valores inseridos maiores do que 3")
    elif int(jogada[0]) < 1 or int(jogada[1]) < 1:
        raise Exception("Jogada inválida: Valores inseridos menores do que 3")
