from functools import reduce


def print_string(string, **kwargs):
    if "backwards" in kwargs:
        if kwargs["backwards"]:
            print(string[::-1])
            return None
        else:
            print(string)
            return None
    else:
        print(string)
        return None


def add(*numbers):
    acc = 0
    for num in numbers:
        acc += num
    return acc


print_string("Olá pessoas", backwards=True)

lista = [1, 2, 3, 4, 5]
tuplo = (1, 2, 3, 4, 5)
dicionario = {"chave": "valor", "chave1": "valor1"}
dict1 = {"chave2": "valor2"}

x, *y, b = lista

print(x, y, b)

print(add(*lista))
merge_dict = {**dicionario, **dict1}
print(merge_dict)


lista = [1, 2, 3, 4, 5]
map_test = map(lambda n: n*2, lista)
print(list(map_test))

lista_filtrada = list(filter(lambda n: n > 3, lista))
print(lista_filtrada)

lista_reduced = reduce(lambda num1, num2: num1 + num2, lista)
print(lista_reduced)


class Disciplina:
    num_disciplinas = 3

    def __init__(self, nome):
        self.nome = nome
        self.lista_alunos = []

    def __str__(self):
        return "This is a class go look somewhere else"

    def add_aluno(self, *alunos):
        for aluno in alunos:
            self.lista_alunos.append(aluno)

    def print_alunos(self):
        print(*self.lista_alunos, sep=",")


class Aluno:
    def __init__(self, nome):
        self.nome = nome

    def __str__(self):
        return self.nome


disc = Disciplina("Física")
aluno1 = Aluno("Jeremias")
print(disc)
disc.add_aluno(aluno1)
disc.add_aluno(Aluno("Catarina"))
disc.print_alunos()