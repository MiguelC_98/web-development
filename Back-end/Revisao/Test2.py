class Animal:

    def __init__(self, name, sound):
        self.name = name
        self.sound = sound

    def speak(self):
        print(self.sound)


class Dog(Animal):

    def __init__(self, name):
        super().__init__(name, "woof")

    def __str__(self):
        return self.name


class Cat(Animal):

    def __init__(self, name):
        super().__init__(name, "miao")

    def __str__(self):
        return self.name


dog = Dog("Cookie")
cat = Cat("Kiko")
print(dog)
print(cat)
dog.speak()
cat.speak()


class NegativeNumberError(Exception):
    def __init__(self):
        self.message = "Ocorreu um erro"


def somar_numeros_negativos(*numeros):
    acc = 0
    for num in numeros:
        if num < 0:
            raise NegativeNumberError
        else:
            acc += num
    return acc


nums = [1, 2, 3, 4, 5]
neg_nums = [-1, -2, -3, -4, -5]
print(somar_numeros_negativos(*nums))
# print(somar_numeros_negativos(*neg_nums))


def somar_nums_consola():
    is_valid = False
    while not is_valid:
        nums = input("Insira os números a somar: ")
        nums = nums.split(" ")
        nums_filtered = [int(num) for num in nums]
        try:
            print(somar_numeros_negativos(*nums_filtered))
            is_valid = True
        except NegativeNumberError:
            print("Erro: Número negativo")


somar_nums_consola()


