class InterfaceGráfica:
    def print_board(self, rows):
        print("\n")
        print("\t  1   |  2  |  3  ")
        print("\t      |     |")
        print("\t1  {}  |  {}  |  {}".format(rows[0][0], rows[0][1], rows[0][2]))
        print('\t______|_____|_____')

        print("\t      |     |")
        print("\t2  {}  |  {}  |  {}".format(rows[1][0], rows[1][1], rows[1][2]))
        print('\t______|_____|_____')

        print("\t      |     |")

        print("\t3  {}  |  {}  |  {}".format(rows[2][0], rows[2][1], rows[2][2]))
        print("\t      |     |")
        print("\n")





class TicTacToe:
    rows = [[" ", " ", " "],
            [" ", " ", " "],
            [" ", " ", " "]]

    def __init__(self, x_nick, o_nick):
        self.nicks = [x_nick, o_nick]
        self.read_wins()
        self.print_board(self.rows)
        self.jogar(self.nicks[0], "X")

    def read_wins(self):
        try:
            with open("winners.txt") as file:
                print("\nSome of the winners are:")
                wins_list=[]
                for line in file:
                    wins_list.append(line)
                n=0
                for line in wins_list:
                    print(line)
                    n+=1
                    if n >= 5:
                        break

        except FileNotFoundError:
            pass

    def check_win(self, rows):
        if rows[0][0] == rows[0][1] and rows[0][0] == rows[0][2] and rows[0][0] != " ":
            return True
        elif rows[1][0] == rows[1][1] and rows[1][0] == rows[1][2] and rows[1][0] != " ":
            return True
        elif rows[2][0] == rows[2][1] and rows[2][0] == rows[2][2] and rows[2][0] != " ":
            return True
        elif rows[0][0] == rows[1][1] and rows[0][0] == rows[2][2] and rows[0][0] != " ":
            return True
        elif rows[0][2] == rows[1][1] and rows[0][2] == rows[2][0] and rows[0][2] != " ":
            return True
        elif rows[0][0] == rows[1][0] and rows[0][0] == rows[2][0] and rows[0][0] != " ":
            return True
        elif rows[0][1] == rows[1][1] and rows[0][1] == rows[2][1] and rows[0][1] != " ":
            return True
        elif rows[0][2] == rows[1][2] and rows[0][2] == rows[2][2] and rows[0][2] != " ":
            return True
        else:
            return False

    def board_full(self, rows):
        check=[]
        for row in rows:
            if row[0] != " " and row[1] != " " and row[2] != " ":
                check.append(1)
        if len(check) > 2:
            if check[0] == 1 and check[1] == 1 and check[2] == 1:
                return True
            else:
                return False
        else:
            return False

    def jogar(self, nick, player):
        import exceptions as exp
        is_valid = False
        while not is_valid:
            jogada_string= "{} é a sua vez! Insira o número da fila, depois o da coluna, que quer jogar:".format(nick)
            jogada = input(jogada_string)
            jogada = jogada.split(" ")
            try:
                exp.exception_check(jogada)
            except Exception as Text:
                print(Text)
                return self.jogar(nick, player)
            jogada=[int(jogada[0])-1,int(jogada[1])-1]
            is_valid=True
        if self.rows[jogada[0]][jogada[1]]== " ":
            self.rows[jogada[0]][jogada[1]] = player
            self.print_board(self.rows)
            win = self.check_win(self.rows)
            full_board=self.board_full(self.rows)
            if win:
                print("The winner is", nick, "!")
                self.winners(nick)
                return ""
            elif full_board:
                print("It's a tie")
                return ""
            elif nick == self.nicks[0]:
                return self.jogar(self.nicks[1], "O")
            else:
                return self.jogar(self.nicks[0], "X")
        else:
            print("Jogada não válida")
            self.print_board(self.rows)
            return self.jogar(nick, player)

    def winners(self,nick):
        try:
            with open("winners.txt") as file:
                dict_linhas = dict()
                for line in file:
                    linha_tot = str(line).split(" ")
                    dict_linhas[linha_tot[0]] = int(linha_tot[1])
                if nick in dict_linhas.keys():
                    dict_linhas[nick] += 1
                else:
                    dict_linhas[nick] = 1
            dict_linhas={k: v for k, v in sorted(dict_linhas.items(), key=lambda item: item[1], reverse=True)}
            with open("winners.txt","w") as file:
                linhas = ""
                for items in dict_linhas.items():
                    linha = "{} {}\n".format(items[0],items[1])
                    linhas += linha
                file.write(linhas)

        except FileNotFoundError:
            with open("winners.txt", "w") as file:
                winner_string="{} 1".format(nick)
                file.write(winner_string)

    def print_board(self, rows):
        print("\n")
        print("\t  1   |  2  |  3  ")
        print("\t      |     |")
        print("\t1  {}  |  {}  |  {}".format(rows[0][0], rows[0][1], rows[0][2]))
        print('\t______|_____|_____')

        print("\t      |     |")
        print("\t2  {}  |  {}  |  {}".format(rows[1][0], rows[1][1], rows[1][2]))
        print('\t______|_____|_____')

        print("\t      |     |")

        print("\t3  {}  |  {}  |  {}".format(rows[2][0], rows[2][1], rows[2][2]))
        print("\t      |     |")
        print("\n")


x_nick = str(input("Jogador X, insira o seu nickname: "))
o_nick = str(input("Jogador O, insira o seu nickname: "))
TicTacToe(x_nick, o_nick)
