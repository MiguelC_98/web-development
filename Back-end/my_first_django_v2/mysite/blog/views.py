from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, EmptyPage,\
                                  PageNotAnInteger
from django.utils import timezone
from django.views.generic import ListView
from taggit.models import Tag
from .models import Post, Comment
from .forms import CommentForm, SearchForm

@login_required
def search(request):
    if request.method == 'GET':
        query = request.GET.get("query")
        results = Post.published.filter(title__contains=query) | Post.published.filter(body__contains=query)
        # title_query = Post.published.filter(title__contains=query)
        # body_query = Post.published.filter(body__contains=query)
        # results = title_query.union(body_query)
        return render(request,
                      'blog/post/search.html',
                      {'results': results,
                       'query':query}
                      )



@login_required
def post_list(request, tag_slug=None):

    num_visitas = request.session.get("num_visitas", 0)

    #num_visitas = int(request.COOKIES.get("num_visitas", 0))
    num_visitas += 1
    request.session["num_visitas"] = num_visitas
    object_list = Post.published.all()
    tag = None

    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        object_list = object_list.filter(tags__in=[tag])

    paginator = Paginator(object_list, 3) # 3 posts in each page
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        posts = paginator.page(paginator.num_pages)
    response = render(request,
                 'blog/post/list.html',
                 {'page': page,
                  'posts': posts,
                  'tag': tag})

    #response.set_cookie("num_visitas", num_visitas)
    return response

@login_required
def post_detail(request, year, month, day, post):
    last_visited_posts = request.session.get("last_visited_posts", [])
    post = get_object_or_404(Post, slug=post,
                                   status='published',
                                   publish__year=year,
                                   publish__month=month,
                                   publish__day=day)
    post_detail = [post.slug, year, month, day]
    found_post = False
    if len(last_visited_posts) > 5:
        last_visited_posts.pop()
    else:
        pass
    for n in last_visited_posts:
        if post.title in n.keys():
            i = last_visited_posts.index(n)
            last_visited_posts.pop(i)
            last_visited_posts.insert(0, {str(post): post_detail})
            found_post = True
        else:
            pass
    if not found_post:
        last_visited_posts.insert(0, {str(post): post_detail})

    request.session["last_visited_posts"] = last_visited_posts

    new_comment = None
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST) # prepopular o fomulário com os dados do comentários
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.post = post
            new_comment.save()
            return redirect(post)
    elif request.method == 'GET':
        comment_form = CommentForm()

    post_tags_ids = post.tags.values_list('id', flat=True)
    similar_posts = Post.published.filter(tags__in=post_tags_ids).exclude(id=post.id)
    similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by('-same_tags', '-publish')[:4]

    response = render(request,
                  'blog/post/detail.html',
                  {'post': post,
                   'comment_form': comment_form,
                   'new_comment': new_comment,
                    'similar_posts': similar_posts
                   })
    return response


class PostListView(ListView):
    queryset = Post.published.all()
    context_object_name = 'posts'
    paginate_by = 3
    template_name = 'blog/post/list.html'
