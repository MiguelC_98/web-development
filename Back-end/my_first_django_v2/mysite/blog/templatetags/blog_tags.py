import markdown
from django import template
from django.utils.safestring import mark_safe
from ..models import Post
from ..forms import SearchForm

register = template.Library()

@register.simple_tag
def total_posts():
    return Post.published.count()

@register.inclusion_tag('blog/post/latest_posts.html')
def show_latest_posts(count=5):
    latest_posts = Post.published.order_by("-publish")[:count]
    return {"latest_posts": latest_posts}

@register.filter(name="markdown")
def markdown_format(text):
    return mark_safe(markdown.markdown(text))

@register.inclusion_tag('blog/post/search_bar.html')
def search_bar():
    search_form = SearchForm()
    return {"search_form":search_form}

@register.filter(name='post_filter')
def posts_pretty(post):
    for k in post.keys():
        return str(k)

@register.filter(name='post_url')
def posts_url(post):
    for i in post.values():
        url = str(i[1])+'/'+str(i[2])+"/"+str(i[3])+"/"+str(i[0])
        return url
