from django.contrib import admin
from .models import Post, Comment


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'author', 'publish', 'status')
    list_filter = ('status', 'created', 'publish', 'author')
    search_fields = ('title', 'body')
    prepopulated_fields = {'slug': ('title',)}
    raw_id_fields = ('author',)
    date_hierarchy = 'publish'
    ordering = ('status', 'publish')


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    fields = ('post', 'active', 'name', 'email', 'body') # ordem dos campos na edição
    list_display = ('post', 'active', 'name', 'email', 'body') # ordem dos campos na lista
    list_filter = ('active', 'created', 'updated') # filtros para a lista (na lateral direita)
    search_fields = ('name', 'email', 'body', 'post__title') # parâmetros que podes pesquisar por
    date_hierarchy = 'updated'
