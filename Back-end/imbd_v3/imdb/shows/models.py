from django.db import models
from django.urls import reverse
from staff.models import Actors, Producers, Writers
from taggit.managers import TaggableManager


class Show(models.Model):
    name = models.CharField(max_length=250)
    rating = models.FloatField()
    air_date = models.DateField()
    actors = models.ManyToManyField(Actors)
    producers = models.ManyToManyField(Producers)
    writers = models.ManyToManyField(Writers)
    description = models.TextField()
    seasons = models.IntegerField()
    poster = models.ImageField(upload_to="shows/")
    tags = TaggableManager()

    def get_absolute_url(self):
        return reverse('shows:show_detail',
                       args=[self.id, ])

    def __str__(self):
        return self.name


class Episode(models.Model):
    show = models.ForeignKey(Show,
                             on_delete=models.CASCADE,
                             related_name='episodes')
    name = models.CharField(max_length=250)
    air_date = models.DateField()
    runtime = models.CharField(max_length=10)
    rating = models.FloatField()
    season = models.IntegerField()
    number = models.IntegerField()
    actors = models.ManyToManyField(Actors)
    producers = models.ManyToManyField(Producers)
    writers = models.ManyToManyField(Writers)
    poster = models.ImageField(upload_to="shows/episodes")
    description = models.TextField()

    class Meta:
        unique_together = (('show', 'season', 'number'),)

    def get_absolute_url(self):
        return reverse('shows:episode_detail',
                       args=[self.show.id, self.season, self.number])

    def __str__(self):
        return self.name
