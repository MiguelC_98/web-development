from django.contrib import admin
from shows.models import Show, Episode

admin.site.register(Show)
admin.site.register(Episode)
