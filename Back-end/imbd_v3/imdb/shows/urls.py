from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views

app_name = 'shows'

urlpatterns = [
    path('', views.show_list, name='show_list'),
    path('<int:id>/', views.show_detail, name='show_detail'),
    path('<int:id>/<int:season>/<int:number>/', views.episode_detail, name='episode_detail'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)