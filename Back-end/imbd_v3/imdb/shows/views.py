from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, get_object_or_404
from .models import Show, Episode


def search(request):
    if request.method == 'GET':
        query = request.GET.get("query")
        results = Show.objects.filter(title__contains=query)

        return render(request,
                      'movies/movies/search.html',
                      {'results': results,
                       'query': query}
                      )


def show_list(request):
    object_list = Show.objects.all()
    paginator = Paginator(object_list, 3)
    page = request.GET.get('page')
    try:
        shows = paginator.page(page)
    except PageNotAnInteger:
        shows = paginator.page(1)
    except EmptyPage:
        shows = paginator.page(paginator.num_pages)
    return render(request,
                  'shows/list.html',
                  {'page': page,
                   'shows': shows})


def show_detail(request, id):
    show = get_object_or_404(Show, id=id)
    episodes_not_ready = list(show.episodes.all())
    episodes = dict()
    for ep in episodes_not_ready:
        if str(ep.season) in episodes:
            episodes[str(ep.season)].append(ep)
        else:
            episodes[str(ep.season)] = [ep]
    print(episodes)

    return render(request,
                  'shows/detail.html',
                  {'show': show,
                   'episodes': episodes})


def episode_detail(request, id, season, number):
    episode = get_object_or_404(Episode, season=season, number=number)
    return render(request,
                  'shows/episodes/detail.html',
                  {'episode': episode})

