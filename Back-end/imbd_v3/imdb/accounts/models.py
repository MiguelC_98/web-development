from django.conf import settings
from django.db import models
from movies.models import Movie
from staff.models import Actors

class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="profile")
    date_of_birth = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to='users/%Y/%m/%d/', blank=True)
    top_movies = models.ManyToManyField(Movie,
                                        blank=True,
                                        related_name="top_movies")
    top_actors = models.ManyToManyField(Actors,
                                        blank=True,
                                        related_name="top_actors")
    def __str__(self):
        return f'Profile for user {self.user.username}'
