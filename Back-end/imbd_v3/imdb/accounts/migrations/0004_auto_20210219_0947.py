# Generated by Django 3.1.7 on 2021-02-19 09:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0002_auto_20210219_0939'),
        ('accounts', '0003_auto_20210219_0933'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='top_movies',
            field=models.ManyToManyField(blank=True, related_name='seen_recently', to='movies.Movie'),
        ),
    ]
