# Generated by Django 3.1.7 on 2021-02-19 09:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0001_initial'),
        ('accounts', '0002_auto_20210219_0920'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='top_movies',
            field=models.ManyToManyField(blank=True, to='movies.Movie'),
        ),
    ]
