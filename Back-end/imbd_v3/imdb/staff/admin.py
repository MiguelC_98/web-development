from django.contrib import admin
from .models import Movie_Staff, Actors, Producers, Writers

admin.site.register(Movie_Staff)
admin.site.register(Actors)
admin.site.register(Producers)
admin.site.register(Writers)
