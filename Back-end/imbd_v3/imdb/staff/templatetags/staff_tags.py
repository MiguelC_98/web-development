from django import template
from ..models import Actors, Writers, Producers
from ..forms import SearchForm

register = template.Library()

@register.simple_tag
def actors_check(id, gender):
    try:
        query = Actors.objects.get(details=id)
        if gender == "Female":
            return "Actress"
        else:
            return "Actor"
    except:
        return ""

@register.simple_tag
def producers_check(id):
    try:
        query = Producers.objects.get(details=id)
        return "Producer"
    except:
        return ""

@register.simple_tag
def writers_check(id):
    try:
        query = Writers.objects.get(details=id)
        return "Writer"
    except:
        return ""


@register.inclusion_tag('staff/search_bar.html')
def search_staff():
    search_form = SearchForm()
    return {"search_form": search_form}
