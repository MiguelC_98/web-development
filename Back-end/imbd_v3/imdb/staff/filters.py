from staff.models import Movie_Staff
import django_filters

class StaffFilter(django_filters.FilterSet):
    class Meta:
        model = Movie_Staff
        fields = ['gender', ]