from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, get_object_or_404
from .models import Movie_Staff,Actors,Writers,Producers
from .filters import StaffFilter


def search(request):
    if request.method == 'GET':
        query = request.GET.get("query")
        results = Movie_Staff.objects.filter(name__contains=query) | Movie_Staff.objects.filter(last_name__contains=query)

        return render(request,
                      'staff/search.html',
                      {'results': results,
                       'query': query}
                      )


def staff_list(request):
    object_list = Movie_Staff.objects.all()
    paginator = Paginator(object_list, 3) # 3 posts in each page
    page = request.GET.get('page')
    try:
        staff_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        staff_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        staff_list = paginator.page(paginator.num_pages)
    actors = Actors.objects.all()
    writers = Writers.objects.all()
    producers = Producers.objects.all()
    return render(request,
                 'staff/staff_list.html',
                 {'page': page,
                  'staff_list': staff_list,
                  'actors': actors,
                  'writers': writers,
                  'producers': producers})


def staff_detail(request, id):
    staff = get_object_or_404(Movie_Staff,
                              pk=id)
    return render(request,
                  'staff/staff_detail.html',
                  {"staff": staff})


def actor_list(request):
    actors_list=[]
    for n in list(Actors.objects.all()):
        actors_list.append(n.details)
    object_list = actors_list
    paginator = Paginator(object_list, 3) # 3 posts in each page
    page = request.GET.get('page')
    try:
        actors_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        actors_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        actors_list = paginator.page(paginator.num_pages)
    actors = Actors.objects.all()
    writers = Writers.objects.all()
    producers = Producers.objects.all()
    return render(request,
                 'staff/actors.html',
                 {'page': page,
                  'actors_list': actors_list,
                  'actors': actors,
                  'writers': writers,
                  'producers': producers})


def producer_list(request):
    producers_list = []
    for n in list(Producers.objects.all()):
        producers_list.append(n.details)
    object_list = producers_list
    paginator = Paginator(object_list, 3) # 3 posts in each page
    page = request.GET.get('page')
    try:
        producers_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        producers_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        producers_list = paginator.page(paginator.num_pages)
    actors = Actors.objects.all()
    writers = Writers.objects.all()
    producers = Producers.objects.all()
    return render(request,
                 'staff/producers.html',
                 {'page': page,
                  'actors': actors,
                  'producers_list': producers_list,
                  'writers': writers,
                  'producers': producers})

def writer_list(request):
    writers_list = []
    for n in list(Writers.objects.all()):
        writers_list.append(n.details)
    object_list = writers_list
    paginator = Paginator(object_list, 3) # 3 posts in each page
    page = request.GET.get('page')
    try:
        writers_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        writers_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        writers_list = paginator.page(paginator.num_pages)
    actors = Actors.objects.all()
    writers = Writers.objects.all()
    producers = Producers.objects.all()
    return render(request,
                 'staff/writers.html',
                 {'page': page,
                  'actors': actors,
                  'writers_list': writers_list,
                  'writers': writers,
                  'producers': producers})

def male_staff(request):
    male_list = []
    for n in list(Movie_Staff.objects.all()):
        if n.gender == "Male":
            male_list.append(n)
    object_list = male_list
    paginator = Paginator(object_list, 3) # 3 posts in each page
    page = request.GET.get('page')
    try:
        male_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        male_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        male_list = paginator.page(paginator.num_pages)
    actors = Actors.objects.all()
    writers = Writers.objects.all()
    producers = Producers.objects.all()
    return render(request,
                 'staff/male_staff.html',
                 {'page': page,
                  'male_list': male_list,
                  'actors': actors,
                  'writers': writers,
                  'producers': producers})


def female_staff(request):
    female_list = []
    for n in list(Movie_Staff.objects.all()):
        if n.gender == "Female":
            female_list.append(n)
    object_list = female_list
    paginator = Paginator(object_list, 3) # 3 posts in each page
    page = request.GET.get('page')
    try:
        female_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        female_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        female_list = paginator.page(paginator.num_pages)
    actors = Actors.objects.all()
    writers = Writers.objects.all()
    producers = Producers.objects.all()
    return render(request,
                 'staff/female_staff.html',
                 {'page': page,
                  'female_list': female_list,
                  'actors': actors,
                  'writers': writers,
                  'producers': producers})
