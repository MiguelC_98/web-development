from django.db import models
from django.urls import reverse
from taggit.managers import TaggableManager


class Movie_Staff(models.Model):
    GENDER_CHOICES = (('male', 'Male'),
                      ('female', 'Female'))
    name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250, default="")
    gender = models.CharField(max_length=20, default="",choices=GENDER_CHOICES)
    bio = models.TextField()
    date_of_birth = models.DateField()
    image = models.ImageField(upload_to='staff/%Y/%m/%d', blank=True)

    def __str__(self):
        return self.name + " " + self.last_name

    def get_absolute_url(self):
        return reverse('staff:staff_detail',
                       args=[self.id])

class Writers(models.Model):
    details = models.OneToOneField(Movie_Staff,
                                on_delete=models.CASCADE,
                                primary_key=True)

    def __str__(self):
        return self.details.name + " " + self.details.last_name


class Actors(models.Model):
    details = models.OneToOneField(Movie_Staff,
                                on_delete=models.CASCADE,
                                primary_key=True)
    def __str__(self):
        return self.details.name + " " + self.details.last_name

class Producers(models.Model):
    details = models.OneToOneField(Movie_Staff,
                                on_delete=models.CASCADE,
                                primary_key=True)

    def __str__(self):
        return self.details.name + " " + self.details.last_name


