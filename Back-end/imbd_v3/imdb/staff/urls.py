from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views

app_name = 'staff'

urlpatterns = [
    path("", views.staff_list, name='staff_list'),
    path("actors/", views.actor_list, name='actor_list'),
    path("producers/", views.producer_list, name='producer_list'),
    path("writers/", views.writer_list, name='writer_list'),
    path("search/", views.search, name='search'),
    path("<int:id>", views.staff_detail, name='staff_detail'),
    path("male/", views.male_staff, name='male_list'),
    path("female/", views.female_staff, name='female_list')
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)