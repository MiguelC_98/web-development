from django import template
from movies.forms import SearchForm


register = template.Library()

@register.inclusion_tag('movies/movies/search_bar.html')
def search_bar():
    search_form = SearchForm()
    return {"search_form": search_form}

@register.filter(name="round_num")
def round_num(num):
    rounded_num = round(num, 2)
    return rounded_num
