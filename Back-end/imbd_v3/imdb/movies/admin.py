from django.contrib import admin
from .models import Movie, Critics


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'rating', 'release_date')
    list_filter = ('release_date', 'rating')
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title',)


admin.site.register(Critics)