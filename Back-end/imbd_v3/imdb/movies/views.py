from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, get_object_or_404, redirect
from taggit.models import Tag
from .models import Movie
from .forms import CriticForm


def search(request):
    if request.method == 'GET':
        query = request.GET.get("query")
        results = Movie.objects.filter(title__contains=query)

        return render(request,
                      'movies/movies/search.html',
                      {'results': results,
                       'query': query}
                      )


def movie_list(request, tag_slug=None):
    tag = None
    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        object_list = Movie.objects.filter(tags__in=[tag])
    else:
        object_list = Movie.objects.all()
    paginator = Paginator(object_list, 3) # 3 posts in each page
    page = request.GET.get('page')
    try:
        movies = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        movies = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        movies = paginator.page(paginator.num_pages)
    return render(request,
                 'movies/movies/list.html',
                 {'page': page,
                  'movies': movies,
                  'tag': tag})


def movie_detail(request, movie_slug, year, month, day):
    movie = get_object_or_404(Movie, slug=movie_slug,
                                   release_date__year=year,
                                   release_date__month=month,
                                   release_date__day=day)
    critic_form = None
    new_critic = None

    list_ratings = []
    for crit in movie.critics.all():
        list_ratings.append(crit.rating)
    sum_of_crits = sum(list_ratings)
    print(list_ratings)

    if request.user.is_authenticated:
        recently_seen_movies = list(request.session.get('recently_seen_movies', []))
        if movie.id in recently_seen_movies:
            i = recently_seen_movies.index(movie.id)
            recently_seen_movies.pop(i)
        recently_seen_movies.insert(0, movie.id)
        if len(recently_seen_movies) > 3:
            recently_seen_movies.pop()
        request.session['recently_seen_movies'] = recently_seen_movies
        if request.method == 'POST':
            critic_form = CriticForm(data=request.POST)
            if critic_form.is_valid():
                new_critic = critic_form.save(commit=False)
                new_critic.movie = movie
                if len(list_ratings) == 0:
                    new_rating = (movie.rating + new_critic.rating)/2
                else:
                    new_rating = (new_critic.rating + sum_of_crits) / (1+len(list_ratings))
                movie.rating = new_rating
                movie.save()
                new_critic.user_id = request.user.id
                new_critic.save()
                return redirect(movie)
        elif request.method == 'GET':
            critic_form = CriticForm()
    else:
        pass
    print(movie.actors.all())
    response = render(request,
                  'movies/movies/detail.html',
                  {'movie': movie,
                   'critic_form': critic_form,
                   'new_critic': new_critic})

    return response


@login_required
def what_to_watch(request):
    user = request.user
    top_movies = list(user.profile.top_movies.all())
    movies_based_on_top = dict()
    for m in top_movies:
        for t in list(m.tags.all()):
            objects = Movie.objects.filter(tags__in=[t])
            for object in objects:
                if m.title in movies_based_on_top.keys():
                    if object in movies_based_on_top[str(m.title)]:
                        pass
                    else:
                        movies_based_on_top[str(m.title)].append(object)
                else:
                    movies_based_on_top[str(m.title)] = [object]
    return render(request,
                  'movies/movies/what_to_watch.html',
                  {'movies_based_on_top': movies_based_on_top})

def what_to_watch_rec(request):
    recently_seen = request.session.get('recently_seen_movies', [])
    movie_list = Movie.objects.all()
    recently_seen_movies = []
    for movie in movie_list:
        for rec_id in recently_seen:
            if movie.id == rec_id:
                recently_seen_movies.append(movie)
            else:
                pass
    movies_based_on_recent = dict()
    for m in recently_seen_movies:
        for t in list(m.tags.all()):
            objects = Movie.objects.filter(tags__in=[t])
            for object in objects:
                if m.title in movies_based_on_recent.keys():
                    if object in movies_based_on_recent[str(m.title)]:
                        pass
                    else:
                        movies_based_on_recent[str(m.title)].append(object)
                else:
                    movies_based_on_recent[str(m.title)] = [object]
    return render(request,
                  'movies/movies/what_to_watch_rec.html',
                  {'movies_based_on_recent': movies_based_on_recent})
