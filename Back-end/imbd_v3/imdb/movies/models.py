from django.db import models
from django.urls import reverse
from staff.models import Actors, Writers, Producers
from django.contrib.auth.models import User
from taggit.managers import TaggableManager


class Movie(models.Model):
    title = models.CharField(max_length=250)
    slug = models.SlugField(default="")
    description = models.TextField()
    rating = models.FloatField()
    duration = models.CharField(max_length=8)
    tags = TaggableManager()
    release_date = models.DateField()
    actors = models.ManyToManyField(Actors)
    writers = models.ManyToManyField(Writers)
    producers = models.ManyToManyField(Producers)
    images = models.ImageField(upload_to='movies/%Y/%m/%d/', blank=True)

    def get_absolute_url(self):
        return reverse('movies:movie_detail',
                       args=[self.release_date.year,
                             self.release_date.month,
                             self.release_date.day,
                             self.slug])

    def __str__(self):
        return self.title


class Critics(models.Model):
    movie = models.ForeignKey(Movie,
                              on_delete=models.CASCADE,
                              related_name="critics")
    critic = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    rating = models.FloatField()
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name="critics")

    def __str__(self):
        return f'Critic to {self.movie} by {self.user}'


