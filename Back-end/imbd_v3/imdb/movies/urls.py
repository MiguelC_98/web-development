from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views

app_name = 'movies'

urlpatterns = [
    path('', views.movie_list, name='movie_list'),
    path('<slug:tag_slug>', views.movie_list, name='movie_list_by_tag'),
    path('<int:year>/<int:month>/<int:day>/<slug:movie_slug>/', views.movie_detail, name='movie_detail'),
    path('search/', views.search, name='search'),
    path('what-to-watch/', views.what_to_watch, name='what-to-watch'),
    path('what-to-watch-rec/', views.what_to_watch_rec, name='what-to-watch-rec')
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)