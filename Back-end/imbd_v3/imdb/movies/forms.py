from django import forms
from .models import Critics
from django.core.exceptions import ValidationError


class SearchForm(forms.Form):
    query = forms.CharField(max_length=30, label='Search here:')


class CriticForm(forms.ModelForm):
    class Meta:
        model = Critics
        fields = ('critic', 'rating')

    def clean(self):
        cleaned_data = super().clean()
        rating = cleaned_data.get('rating')
        if rating > 10 or rating < 1:
            raise forms.ValidationError('Rating needs to be between 1 and 10, not more, not less.')
        return cleaned_data
