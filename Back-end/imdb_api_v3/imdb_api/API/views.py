from django.contrib.auth.models import User
from django.shortcuts import render
from rest_framework import generics, status
from API.models import Movie, Actor, Producer, Writer, MovieStaff, CriticMovie, Show, Episode, CriticShow
from API.serializers import MovieSerializer, ActorSerializer, ProducerSerializer, WriterSerializer, StaffSerializer, CriticMovieSerializer, UserSerializer, ShowSerializer, EpisodeSerializer, CriticShowSerializer, NewEpisodeSerializer
from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.filters import SearchFilter
from .filters import EpFilters


class MovieList(generics.ListCreateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    name = 'movie-list'
    search_fields = (
        '^title',
    )


class MovieDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    name = 'movie-detail'


class ActorList(generics.ListCreateAPIView):
    queryset = Actor.objects.all()
    serializer_class = ActorSerializer
    name = 'actor-list'
    search_fields = ('^details__name',)


class ActorDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Actor.objects.all()
    serializer_class = ActorSerializer
    name = 'actor-detail'


class ProducerList(generics.ListCreateAPIView):
    queryset = Producer.objects.all()
    serializer_class = ProducerSerializer
    name = 'producer-list'
    search_fields = ('^details__name',)


class ProducerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Producer.objects.all()
    serializer_class = ProducerSerializer
    name = 'producer-detail'


class WriterList(generics.ListCreateAPIView):
    queryset = Writer.objects.all()
    serializer_class = WriterSerializer
    name = 'writer-list'
    search_fields = ('^details__name',)


class WriterDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Writer.objects.all()
    serializer_class = WriterSerializer
    name = 'writer-detail'


class MovieStaffList(generics.ListCreateAPIView):
    queryset = MovieStaff.objects.all()
    serializer_class = StaffSerializer
    name = 'moviestaff-list'
    search_fields = (
        '^name',
    )


class MovieStaffDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = MovieStaff.objects.all()
    serializer_class = StaffSerializer
    name = 'moviestaff-detail'


class CriticMovieList(generics.ListCreateAPIView):
    queryset = CriticMovie.objects.all()
    serializer_class = CriticMovieSerializer
    name = 'criticmovie-list'


class CriticMovieDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CriticMovie.objects.all()
    serializer_class = CriticMovieSerializer
    name = 'criticmovie-detail'


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    name = 'user-detail'


class ShowList(generics.ListCreateAPIView):
    queryset = Show.objects.all()
    serializer_class = ShowSerializer
    name = 'show-list'
    search_fields = (
        '^name',
    )
    """
    def get_serializer_class(self):
        if request.method == 'GET':
            serializer_class = ShowSerializer
        elif request.method == 'POST':
            serializer_class = ShowPostSerializer
    """


class ShowDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Show.objects.all()
    serializer_class = ShowSerializer
    name = 'show-detail'

@api_view(['GET','POST'])
def episode_list_per_show(request, pk):
    show = get_object_or_404(Show, pk=pk)
    if request.method == 'GET':
        episodes = Episode.objects.filter(show=show)
        epfilter = EpFilters(request.GET, queryset=episodes)
        if epfilter.is_valid():
            episodes = epfilter.qs
        serializer = EpisodeSerializer(episodes, many=True, context={'request': request})
        return Response(serializer.data)
    elif request.method == 'POST':
        show_str = str(reverse('show-list', request=request)) + str(pk)
        request.data['show'] = show_str
        print(request.data)
        serializer = NewEpisodeSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE', 'PATCH', 'PUT'])
def episode_detail_per_show(request, pk, season, number):
    show = get_object_or_404(Show, pk=pk)
    if request.method == 'GET':
        episode = Episode.objects.get(show=show, season=season, number=number)
        serializer = EpisodeSerializer(episode, context={'request': request})
        return Response(serializer.data)


class EpisodeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Episode.objects.all()
    serializer_class = EpisodeSerializer
    name = 'episode-detail'


class CriticShowDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CriticShow.objects.all()
    serializer_class = CriticShowSerializer
    name = 'criticshow-detail'


class APIRoot(generics.GenericAPIView):
    name = 'api-root'

    def get(self, request, *args, **kwargs):
        return Response({
            'movies': reverse(MovieList.name, request=request),
            'shows': reverse(ShowList.name, request=request),
            'staff': reverse(MovieStaffList.name, request=request),
            'actors': reverse(ActorList.name, request=request),
            'producers': reverse(ProducerList.name, request=request),
            'writers': reverse(WriterList.name, request=request),
        })
