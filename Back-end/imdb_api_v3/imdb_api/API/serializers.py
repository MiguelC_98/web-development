from django.contrib.auth.models import User
from django_filters.rest_framework import FilterSet
from rest_framework import serializers
from API.models import Movie, Show, Episode, Actor, Producer, Writer, CriticMovie, CriticShow, MovieStaff


class UserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = User
        fields = (
            'username',
        )


class StaffSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = MovieStaff
        fields = (
            'url',
            'name',
            'last_name',
            'gender',
            'bio',
            'date_of_birth'
        )


class MovieNameSerializers(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Movie
        fields = (
            'url',
            'title'
        )


class ShowNameSerializers(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Show
        fields = (
            'url',
            'name'
        )


class ActorSerializer(serializers.HyperlinkedModelSerializer):

    movies = MovieNameSerializers(many=True)
    shows = ShowNameSerializers(many=True)
    details = StaffSerializer(read_only=True)

    class Meta:
        model = Actor
        fields = (
            'url',
            'details',
            'movies',
            'shows'
        )


class ProducerSerializer(serializers.HyperlinkedModelSerializer):

    movies = MovieNameSerializers(many=True)
    shows = ShowNameSerializers(many=True)
    details = StaffSerializer(read_only=True)

    class Meta:
        model = Producer
        fields = (
            'url',
            'details',
            'movies',
            'shows'
        )


class WriterSerializer(serializers.HyperlinkedModelSerializer):

    movies = MovieNameSerializers(many=True)
    shows = ShowNameSerializers(many=True)
    details = StaffSerializer(read_only=True)

    class Meta:
        model = Writer
        fields = (
            'url',
            'details',
            'movies',
            'shows'
        )


class MovieSerializer(serializers.HyperlinkedModelSerializer):

    """actors = ActorSerializer(many=True)
    producers = ProducerSerializer(many=True)
    writers = WriterSerializer(many=True)"""

    class Meta:
        model = Movie
        fields = (
            'url',
            'pk',
            'title',
            'description',
            'rating',
            'duration',
            'tags',
            'release_date',
            'actors',
            'producers',
            'writers'

        )


class MovieForCritSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Movie
        fields = (
            'url',
            'title',
            'rating'
        )


class CriticMovieSerializer(serializers.HyperlinkedModelSerializer):

    user = UserSerializer()
    movie = MovieForCritSerializer()

    class Meta:
        model = CriticMovie
        fields = (
            'url',
            'movie',
            'created',
            'updated',
            'rating',
            'critic',
            'user'
        )


class EpForShowSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Episode
        fields = (
            'url',
            'season',
            'number',
            'name'
        )


class CritsShowLittleSerializer(serializers.HyperlinkedModelSerializer):

    user = UserSerializer()

    class Meta:
        model = CriticShow
        fields = (
            'url',
            'rating',
            'user'
        )


class ShowSerializer(serializers.HyperlinkedModelSerializer):

    episodes = serializers.HyperlinkedIdentityField(view_name='show-episode-list', read_only=True)
    critics = CritsShowLittleSerializer(many=True)
    """actors = ActorSerializer(many=True)
    producers = ProducerSerializer(many=True)
    writers = WriterSerializer(many=True)"""

    class Meta:
        model = Show
        fields = (
            'url',
            'name',
            'rating',
            'air_date',
            'actors',
            'producers',
            'writers',
            'description',
            'episodes',
            'seasons',
            'tags',
            'critics'
        )


class EpisodeSerializer(serializers.HyperlinkedModelSerializer):

    """show = ShowSerializer(read_only=True)"""
    url = serializers.CharField(source='get_absolute_url', read_only=True)

    class Meta:
        model = Episode
        fields = (
            'url',
            'name',
            'season',
            'number',
            'air_date',
            'runtime',
            'rating',
            'actors',
            'producers',
            'writers',
            'description'
        )


class ShowForCritSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Show
        fields = (
            'url',
            'name',
            'rating'
        )


class CriticShowSerializer(serializers.HyperlinkedModelSerializer):

    show = ShowForCritSerializer()
    user = UserSerializer()

    class Meta:
        model = CriticShow
        fields = (
            'show',
            'created',
            'updated',
            'rating',
            'critic',
            'user'
        )


class NewEpisodeSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Episode
        fields = (
            'show',
            'season',
            'number',
            'air_date',
            'runtime',
            'rating',
            'actors',
            'producers',
            'writers',
            'description'
        )
