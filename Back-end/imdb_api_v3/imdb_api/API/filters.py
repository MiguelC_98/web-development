from django_filters import FilterSet
from .models import Episode


class EpFilters(FilterSet):

    class Meta:
        model = Episode
        fields = ['season', 'number']
