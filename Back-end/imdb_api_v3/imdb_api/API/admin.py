from django.contrib import admin
from . import models

admin.site.register(models.Movie)
admin.site.register(models.MovieStaff)
admin.site.register(models.Show)
admin.site.register(models.Actor)
admin.site.register(models.Producer)
admin.site.register(models.Writer)
admin.site.register(models.Episode)
admin.site.register(models.CriticShow)
admin.site.register(models.CriticMovie)


