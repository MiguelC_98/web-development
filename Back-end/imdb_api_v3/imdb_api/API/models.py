from django.contrib.auth.models import User
from django.db import models
from rest_framework.reverse import reverse


class MovieStaff(models.Model):
    GENDER_CHOICES = (('male', 'Male'),
                      ('female', 'Female'))
    name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250, default="")
    gender = models.CharField(max_length=20, default="", choices=GENDER_CHOICES)
    bio = models.TextField()
    date_of_birth = models.DateField()

    def __str__(self):
        return self.name + " " + self.last_name


class Writer(models.Model):
    details = models.OneToOneField(MovieStaff,
                                on_delete=models.CASCADE,
                                unique=True)

    def __str__(self):
        return self.details.name + " " + self.details.last_name


class Actor(models.Model):
    details = models.OneToOneField(MovieStaff,
                                on_delete=models.CASCADE,
                                unique=True)
    def __str__(self):
        return self.details.name + " " + self.details.last_name


class Producer(models.Model):
    details = models.OneToOneField(MovieStaff,
                                on_delete=models.CASCADE,
                                unique=True)

    def __str__(self):
        return self.details.name + " " + self.details.last_name


class Movie(models.Model):
    title = models.CharField(max_length=250)
    slug = models.SlugField(default="")
    description = models.TextField()
    rating = models.FloatField()
    duration = models.CharField(max_length=8)
    tags = models.CharField(max_length=250)
    release_date = models.DateField()
    actors = models.ManyToManyField(Actor, related_name='movies')
    writers = models.ManyToManyField(Writer, related_name='movies')
    producers = models.ManyToManyField(Producer, related_name='movies')

    def __str__(self):
        return self.title


class Show(models.Model):
    name = models.CharField(max_length=250)
    rating = models.FloatField()
    air_date = models.DateField()
    actors = models.ManyToManyField(Actor, related_name='shows')
    producers = models.ManyToManyField(Producer, related_name='shows')
    writers = models.ManyToManyField(Writer, related_name='shows')
    description = models.TextField()
    seasons = models.IntegerField()
    tags = models.CharField(max_length=250)


    def __str__(self):
        return self.name


class Episode(models.Model):
    show = models.ForeignKey(Show,
                             on_delete=models.CASCADE,
                             related_name='episodes')
    name = models.CharField(max_length=250)
    air_date = models.DateField()
    runtime = models.CharField(max_length=10)
    rating = models.FloatField()
    season = models.IntegerField()
    number = models.IntegerField()
    actors = models.ManyToManyField(Actor)
    producers = models.ManyToManyField(Producer)
    writers = models.ManyToManyField(Writer)
    description = models.TextField()

    class Meta:
        unique_together = (('show', 'season', 'number'),)

    def get_absolute_url(self):
        string = 'http://127.0.0.1' + reverse('show-list') + '{show}/episodes/{season}/{number}'.format(show=self.show_id ,season=self.season, number=self.number)
        return string

    def __str__(self):
        return self.name


class CriticMovie(models.Model):
    movie = models.ForeignKey(Movie,
                              on_delete=models.CASCADE,
                              related_name="critics")
    critic = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    rating = models.FloatField()
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name="critics_movie")

    def __str__(self):
        return f'Critic to {self.movie} by {self.user}'


class CriticShow(models.Model):
    show = models.ForeignKey(Show,
                              on_delete=models.CASCADE,
                              related_name="critics")
    critic = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    rating = models.FloatField()
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name="critics_show")

    def __str__(self):
        return f'Critic to {self.show} by {self.user}'

