# Generated by Django 3.1.7 on 2021-03-18 12:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='actors',
            field=models.ManyToManyField(related_name='movies', to='API.Actor'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='producers',
            field=models.ManyToManyField(related_name='movies', to='API.Producer'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='writers',
            field=models.ManyToManyField(related_name='movies', to='API.Writer'),
        ),
        migrations.AlterField(
            model_name='show',
            name='actors',
            field=models.ManyToManyField(related_name='shows', to='API.Actor'),
        ),
        migrations.AlterField(
            model_name='show',
            name='producers',
            field=models.ManyToManyField(related_name='shows', to='API.Producer'),
        ),
        migrations.AlterField(
            model_name='show',
            name='writers',
            field=models.ManyToManyField(related_name='shows', to='API.Writer'),
        ),
    ]
