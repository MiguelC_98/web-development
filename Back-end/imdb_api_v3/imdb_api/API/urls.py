from django.urls import path
from . import views

urlpatterns = [
    path('', views.APIRoot.as_view(), name=views.APIRoot.name),
    path('movies/', views.MovieList.as_view(), name=views.MovieList.name),
    path('movies/<int:pk>', views.MovieDetail.as_view(), name=views.MovieDetail.name),
    path('shows/', views.ShowList.as_view(), name=views.ShowList.name),
    path('shows/<int:pk>', views.ShowDetail.as_view(), name=views.ShowDetail.name),
    path('shows/<int:pk>/episodes/', views.episode_list_per_show, name='show-episode-list'),
    path('shows/<int:pk>/episodes/<int:season>/<int:number>', views.episode_detail_per_show, name='show-episode-detail'),
    path('shows/episodes/<int:pk>', views.EpisodeDetail.as_view(), name=views.EpisodeDetail.name),
    path('actors/', views.ActorList.as_view(), name=views.ActorList.name),
    path('actors/<int:pk>', views.ActorDetail.as_view(), name=views.ActorDetail.name),
    path('producers/', views.ProducerList.as_view(), name=views.ProducerList.name),
    path('producers/<int:pk>', views.ProducerDetail.as_view(), name=views.ProducerDetail.name),
    path('writers/', views.WriterList.as_view(), name=views.WriterList.name),
    path('writers/<int:pk>', views.WriterDetail.as_view(), name=views.WriterDetail.name),
    path('staff/', views.MovieStaffList.as_view(), name=views.MovieStaffList.name),
    path('staff/<int:pk>', views.MovieStaffDetail.as_view(), name=views.MovieStaffDetail.name),
    path('movie-critics/', views.CriticMovieList.as_view(), name=views.CriticMovieList.name),
    path('movie-critics/<int:pk>', views.CriticMovieDetail.as_view(), name=views.CriticMovieDetail.name),
    path('show-critics/<int:pk>', views.CriticShowDetail.as_view(), name=views.CriticShowDetail.name),
    path('user/<int:pk>', views.UserDetail.as_view(), name=views.UserDetail.name),
]